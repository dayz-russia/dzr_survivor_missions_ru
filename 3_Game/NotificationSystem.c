modded class NotificationSystem
{
	//TODO: You have entered это тайтл! А время это текст. Заменять тайтл нужно.
	override static void SendNotificationToPlayerIdentityExtended( PlayerIdentity player, float show_time, string title_text, string detail_text = "", string icon = "" )
	{
		ScriptRPC rpc = new ScriptRPC();
		//Print("[dzr_survivor_missions_ru] ::: init");

			//Print("title_text: "+title_text);
			//Print("detail_text: "+detail_text);
			/*
			string enString1 = "INFO: A survivor is transmitting via radio on";
			string enString2 = "Mhz. Stay tuned for incoming calls!";
			
			string ruString1 = "ВНИМАНИЕ: На радиочастоте ";
			string ruString2 = "МГц передаётся сообщение от выжившего.";
			
			detail_text.Replace(enString1, ruString1);
			detail_text.Replace(enString2, ruString2);
			*/
			
			
			
			string ZoneEntryMsg_en = "I am almost here! ...";
			string ZoneEntryMsg_ru = "Я почти на месте! ...";
			
			string ZoneEntryMsg2_en = "minutes left before the mission ends!";
			string ZoneEntryMsg2_ru = "мин. до окончания задания.";
			
			string TargetFoundMsg_en = "I have found it.";
			string TargetFoundMsg_ru = "Я нашел цель.";
			
			detail_text.Replace(ZoneEntryMsg_en, ZoneEntryMsg_ru);
			detail_text.Replace(ZoneEntryMsg2_en, ZoneEntryMsg2_ru);
			detail_text.Replace(TargetFoundMsg_en, TargetFoundMsg_ru);
			
			
			
			string string_en = "You have left the mission zone!";
			string string_ru = "Вы вышли из зоны задания!";
			
			detail_text.Replace(string_en, string_ru);
			
			//MessageAction(detail_text);
			
			
			string_en = "I have found a message!";
			string_ru = "Вы нашли послание!";
			
			detail_text.Replace(string_en, string_ru);
			
			
			
			string_en = "I should go to";
			string_ru = "Я должен идти на";
			
			
			detail_text.Replace(string_en, string_ru);
			detail_text.Replace("of" , "");
			
			//dirs
			
			detail_text.Replace("northnorthwest" , "северо-северо-запад");
			detail_text.Replace("northnortheast" , "северо-северо-восток");
			detail_text.Replace("southwest" , "юго-запад");
			detail_text.Replace("northwest" , "северо-запад");
			detail_text.Replace("southeast" , "юго-запад");
			detail_text.Replace("northeast" , "северо-запад");
			
			detail_text.Replace("north" , "север");
			detail_text.Replace("south" , "юг");
			detail_text.Replace("east" , "восток");
			detail_text.Replace("west" , "запад");
			detail_text.Replace("central" , "центр");
			//dirs
			
			
			string_en = "I have to be careful. Only";
			string_ru = "Нужно быть внимательным. Осталось всего";
			string string2_en = "minutes remaining. Let's get it!";
			string string2_ru = "мин. Нужно успеть!";
			
			detail_text.Replace(string_en, string_ru);
			detail_text.Replace(string2_en ,string2_ru);
			
			
			
			string_en = "Somebody has finished the mission!";
			string_ru = "Задание выполнил кто-то другой.";
			
			detail_text.Replace(string_en, string_ru);
			
			
			
			string_en = "Mission timed out. You are too late!";
			string_ru = "Время вышло. Ты опоздал.";
			
			detail_text.Replace(string_en, string_ru);
			
			
			
			
			
			
			string2_en = "mission with some good survival equipment";
			string2_ru = ". Можно добыть неплохое снаряжение.";
			string string1_en = "There is a ";
			string_ru = "Появилось задание: ";
			
			detail_text.Replace(string1_en, string_ru);
			detail_text.Replace(string2_en , string2_ru);
			
			detail_text.Replace("UrbanMall","Магазин");
			detail_text.Replace("Apartment","Квартира");
			detail_text.Replace("BearHunt","Магазин");
			detail_text.Replace("Camp","Лагерь");
			detail_text.Replace("CityMall","Магазин");
			detail_text.Replace("CityStore","Магазин");
			detail_text.Replace("FreePigs","Ферма");
			detail_text.Replace("Ganja","Каннабис");
			detail_text.Replace("Graveyard","Кладбище");
			detail_text.Replace("Horde","Орда");
			detail_text.Replace("PlaneCrash","Крушение");
			detail_text.Replace("Psilos","Грибы");
			detail_text.Replace("Shrooms","Грибы");
			detail_text.Replace("Transport","Транспорт");
			
			//MSG
			title_text.Replace("You have entered the mission zone!", "Вы вошли в зону задания." );
			detail_text.Replace("minutes left until the mission ends.", "мин. до окончания задания." );
			title_text.Replace("You have left the mission zone!", "Вы вышли из зоны задания." );
			detail_text.Replace("Can anybody hear me? Over!", "Меня кто-нибудь слышит? Приём." );
			title_text.Replace("You have found the target!", "Вы нашли цель задания." );
			detail_text.Replace("minutes left. Over and out!", "мин. осталось. Приём." );
			detail_text.Replace("Thank you survivor for solving that.\nI will call you for further tasks on this radio channel.\nOver and out!", "Спасибо за помощь, выживший.\nЯ свяжусь с тобой на этой частоте, если появятся ещё задания.\nОтбой." );
			detail_text.Replace("I am sorry but you are too late!\nI will call you for further tasks on this radio channel.\nOver and out!", "К сожалению ты опоздал.\nЯ свяжусь с тобой на этой частоте, если появятся ещё задания.\nОтбой." );
			//MSG
			
			if ( detail_text.Contains("Dr. Legasov, leading") )
			{
			title_text = "Истории выживших";
				detail_text = "Доктор Легасов, ведущий учёный в Курчатовском Институте Ядерной Физики, был отправлен на разрушенную АЭС для расследования причин аварии и чтобы предотвратить радиационное заражение. Легасов был так же химиком и проводил самостоятельные исследования. Он обнаружил неизвестный вирус у диких животных в Чернарусском регионе и у мужчины, работавшего над ликвидации аварии. Легасов пытался его разыскать, но тот успел уехать.";
			}
			
			if ( detail_text.Contains("After the outbreak in Chernarus") )
			{
				title_text = "Истории выживших";
				detail_text = "По приказу властей Легасов едет в Чернарусь и продолжает исследования вируса. Он установил, что инфицированные умирают через час после заражения, но их мозг остаётся способным на низкоуровневые действия. Через два часа они становятся агрессивными и нападают на окружающих. Когда Легасов прибыл в Чернарусь, 20% населения уже было инфицировано. Отмобилизованная армия не справилась с ордами инфицированных.";
			}
			
			if ( detail_text.Contains("Dr. Tamarova, attending physician") )
			{
				title_text = "Истории выживших";
				detail_text = "Доктор Тополева, врач черногорской больницы, лечила пациента из Березино с необъяснимыми симптомами после укуса собаки. Мужчина участвовал в ликвидации аварии на ЧАЭС. У пациента была лёгкая лучевая болезнь, но он испытывал приступы агрессии. Позже заразились две медсестры и напали на пациентов. Спустя 6 часов заразились 400 жителей города, власти объявил чрезвычайное положение.";
			}
			
			
			detail_text.Replace("Balota Airstrip", "Аэродром Балоты" );
			detail_text.Replace("Belaya Polana", "Белая Поляна" );
			detail_text.Replace("Chernaya Polana", "Чёрная Поляна" );
			detail_text.Replace("Kalinovka Military Base", "Военна База в Калиновке" );
			detail_text.Replace("Topolka Dam", "Дамба Тополька" );
			detail_text.Replace("Tishina Dam", "Дамба Тишина" );
			detail_text.Replace("Vysoky Kamen Mountain", "Пик Высокий Камень" );
			
			detail_text.Replace("Chernogorsk", "Черногорск" );
			
			detail_text.Replace("Novoselki", "Новосёлки" );
			detail_text.Replace("Severograd", "Североград" );
			detail_text.Replace("Novaya Petrovka", "Новая Петровка" );
			detail_text.Replace("Vysotovo", "Высотово" );
			
			detail_text.Replace("Bor", "Бор" );
			detail_text.Replace("Dolina", "Долина" );
			
			detail_text.Replace("Drozhino", "Дрожино" );
			detail_text.Replace("Kamensk", "Каменск" );
			detail_text.Replace("Kamyshovo", "Камышово" );
			detail_text.Replace("Khelm", "Хельм" );
			detail_text.Replace("Komarovo", "Комарово" );
			detail_text.Replace("Msta", "Мста" );
			detail_text.Replace("Nadezhdino", "Надеждино" );
			detail_text.Replace("Topolniki", "Топольники" );
			detail_text.Replace("Tulga", "Тулга" );
			detail_text.Replace("Vavilovo", "Вавилово" );
			detail_text.Replace("Vybor", "Выбор" );
			detail_text.Replace("Grishino", "Гришино" );
			detail_text.Replace("Kabanino", "Кабанино" );
			detail_text.Replace("Mogilevka", "Могилёвка" );
			detail_text.Replace("Nizhnoye", "Нижное" );
			detail_text.Replace("Olha", "Ольха" );
			detail_text.Replace("Orlovets", "Орловец" );
			detail_text.Replace("Polesovo", "Полесово" );
			detail_text.Replace("Ratnoe", "Ратное" );
			detail_text.Replace("Sinystok", "Синисток" );
			detail_text.Replace("Staroye", "Старое" );
			detail_text.Replace("Gorka", "Горка" );
			detail_text.Replace("Guglovo", "Гуглово" );
			detail_text.Replace("Gvozdno", "Гвоздно" );
			detail_text.Replace("Nagornoe", "Нагорное" );
			detail_text.Replace("Novy Sobor", "Новый Собор" );
			detail_text.Replace("Pulkovo", "Пулково" );
			detail_text.Replace("Pusta", "Пуста" );
			detail_text.Replace("Shakhovka", "Шаховка" );
			detail_text.Replace("Svergino", "Свергино" );
			detail_text.Replace("Svetlojarsk", "Светлоярск" );
			detail_text.Replace("Kalinovka", "Калиновка" );
			detail_text.Replace("Vyshnoye", "Вышное" );
			detail_text.Replace("Berezino", "Березино" );
			detail_text.Replace("Elektrozavodsk", "Электрозаводск" );
			detail_text.Replace("Kamenka", "Каменка" );
			detail_text.Replace("Krasnostav", "Красностав" );
			detail_text.Replace("Novodmitrovsk", "Новодмитровск" );
			detail_text.Replace("Zelenogorsk", "Зеленогорск" );
			detail_text.Replace("Polana", "Поляна" );
			detail_text.Replace("Pustoshka", "Пустошка" );
			detail_text.Replace("Stary Sobor", "Старый Собор" );
			detail_text.Replace("Devils Castle", "Чёртов Замок" );
			detail_text.Replace("Dobroe", "Доброе" );
			detail_text.Replace("Dubovo", "Дубово" );
			detail_text.Replace("Dubrovka", "Дубровка" );
			detail_text.Replace("Olha Mountain", "Гора Ольха" );
			detail_text.Replace("Black Lake", "Чёрное Озеро" );
			detail_text.Replace("Zub Castle", "Замок Зуб" );
			detail_text.Replace("Kozlovka", "Козловка" );
			detail_text.Replace("Kotka Mountain", "Гора Котька" );
			detail_text.Replace("Ostry Mountain", "Пик Острый" );
			detail_text.Replace("Prud", "Пруд" );
			detail_text.Replace("Rog Castle", "Замок Рог" );
			detail_text.Replace("Zaprudnoe", "Запрудное" );
			detail_text.Replace("Veresnik Mountain", "Пик Вересник" );
			detail_text.Replace("Solnichniy", "Солнечный" );
			detail_text.Replace("Dubina Mountain", "Пик Дубина" );
			detail_text.Replace("Lopatino", "Лопатино" );
		
		rpc.Write(show_time);
		rpc.Write(title_text);
		rpc.Write(detail_text);
		rpc.Write(icon);
		
		rpc.Send(null, ERPCs.RPC_SEND_NOTIFICATION_EXTENDED, true, player);
	};
	
	
}